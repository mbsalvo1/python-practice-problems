# Complete the count_letters_and_digits function which
# accepts a parameter s that contains a string and returns
# two values, the number of letters in the string and the
# number of digits in the string
#
# Examples:
#   * "" returns 0, 0
#   * "a" returns 1, 0
#   * "1" returns 0, 1
#   * "1a" returns 1, 1
#
# To test if a character c is a digit, you can use the
# c.isdigit() method to return True of False
#
# To test if a character c is a letter, you can use the
# c.isalpha() method to return True of False
#
# Remember that functions can return more than one value
# in Python. You just use a comma with the return, like
# this:
#      return value1, value2


#  MY NOTES
# two sums to print one for char and one for digit
# loop to them in string
# return sum1 and sum2

def count_letters_and_digits(s):
    char = 0
    dig = 0
    for value in (s):
        if value.isalpha(): 
            char += 1
        if value.isdigit():
            dig += 1
    return char, dig
    
print(count_letters_and_digits("11123456789abcdefghi"))
